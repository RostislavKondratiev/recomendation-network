import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { ProductDescriptionService } from './product-description.service';
import { Product } from '../../core/models/product.model';

@Injectable()
export class ProductDescriptionResolver implements Resolve<Product> {
  constructor(private descriptionService: ProductDescriptionService) {}
  public resolve(route: ActivatedRouteSnapshot): Promise<Product> {
    return this.descriptionService.getProduct(route.params.id).then((res: Product) => {
      return res;
    });
  }
}
