import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../../core/services/config.service';
import { Product } from '../../core/models/product.model';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ProductDescriptionService {
  constructor(private http: HttpClient, private config: ConfigService) {}

  public getProduct(id: number): Promise<Product> {
    return this.http.get(`${this.config.url}/products/`).map((res) => {
      if (Array.isArray(res)) {
        for (let item of res) {
          if (item.id === id) {
            return item;
          }
        }
      }
    }).toPromise();
  }
}
