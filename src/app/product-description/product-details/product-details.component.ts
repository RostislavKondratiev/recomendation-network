import { Component, Input } from '@angular/core';
import { Product } from '../../core/models/product.model';

@Component({
  selector: 'rn-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent {
}
