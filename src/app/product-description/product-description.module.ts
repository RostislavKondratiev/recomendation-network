import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductDescriptionComponent } from './product-description.component';
import { ProductDescriptionService } from './shared/product-description.service';
import { ProductDescriptionResolver } from './shared/product.description.resolver';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  declarations: [
    ProductDetailsComponent,
    ProductDescriptionComponent
  ],
  providers: [
    ProductDescriptionService,
    ProductDescriptionResolver
  ],
  exports: []
})
export class ProductDescriptionModule {
}
