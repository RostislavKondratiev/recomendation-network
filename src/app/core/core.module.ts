import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from './services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { ConfigService } from './services/config.service';
import { SessionService } from './services/session.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    AuthService,
    ConfigService,
    SessionService
  ],
  exports: []
})
export class CoreModule {
}
