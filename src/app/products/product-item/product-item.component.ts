import { Component, Input, OnInit } from '@angular/core';
@Component({
  selector: 'rn-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {
  @Input() public item;
  private imgPath: string = '../../../assets/img/';
  public ngOnInit() {
    this.item.img = `${this.imgPath + this.item.img}`;
    console.log(this.item);
  }
}
