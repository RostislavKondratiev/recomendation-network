import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../shared/products.service';
import { Product } from '../../core/models/product.model';

@Component({
  selector: 'rn-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  public products: Product[];
  constructor(private productsService: ProductsService ) {}
  public ngOnInit() {
    this.productsService.getProducts().subscribe((res) => {
      this.products = res;
    });
  }
}
